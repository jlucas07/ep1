#ifndef BLOCK_HPP
#define BLOCK_HPP

#include "posicao.hpp"
#include "geradorformas.hpp"

  class Block : public GeradorFormas{

  public:
    Block();
    ~Block();

    void block();
  };

#endif
