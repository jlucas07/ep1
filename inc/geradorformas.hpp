#ifndef GERADORFORMAS_HPP
#define GERADORFORMAS_HPP

#include <iostream>
#include <string>
#include "posicao.hpp"

using namespace std;

class GeradorFormas{

protected:

    bool gerador[50][100];

  public:

    GeradorFormas();
    ~GeradorFormas();

  void Vizinhos();

  void limparTela();

  void gerarForma();
};
#endif
