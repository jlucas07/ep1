#ifndef GLINDERGUN_HPP
#define GLINDERGUN_HPP

#include "geradorformas.hpp"
#include <iostream>

  class GlinderGun : public GeradorFormas{

  public:
    GlinderGun();
    ~GlinderGun();

    void gun();

  };

#endif
