#ifndef POSICAO_HPP
#define POSICAO_HPP

class Posicao {

  private:

    char posx;
    char posy;

  public:

    Posicao();
    Posicao(char x, char y);
    ~Posicao();

    char getPosx();
    void setPosx(char x);

    char getPosy();
    void setPosy(char y);
};

#endif

