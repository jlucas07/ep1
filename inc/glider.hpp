#ifndef GLIDER_HPP
#define GLIDER_HPP

#include "posicao.hpp"
#include "geradorformas.hpp"

  class Glider : public GeradorFormas{

  public:
    Glider();
    ~Glider();

    void glider (Posicao posicao);

  };

  #endif
