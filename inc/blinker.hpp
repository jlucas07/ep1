#ifndef BLINKER_HPP
#define BLINKER_HPP

#include "posicao.hpp"
#include "geradorformas.hpp"

class Blinker : public GeradorFormas{

public:
  Blinker();
  ~Blinker();

  void blinker (Posicao posicao);

};
#endif
