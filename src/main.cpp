#include "posicao.hpp"
#include "geradorformas.hpp"
#include "block.hpp"
#include "glindergun.hpp"
#include "glider.hpp"
#include "blinker.hpp"

#include <string>
#include <iostream>

using namespace std;

  Posicao getPosicao();

  Posicao getPosicao(){

    char linha;
    char coluna;

    cout << "Para incluir a linha digite 'l' ";
    cin >> linha;

    while (linha != 'l'){
      cout << "Apenas a tecla 'l' pode ser inclusa";
      cin >> linha;
    }
    cout << "Para incluir a coluna digite 'c' ";
    cin >> coluna;

    while (coluna != 'c'){
      cout << "Apenas a tecla 'c' pode ser inclusa!";
      cin >> coluna;
    }

    Posicao posicao(linha, coluna);
    return posicao;
    }


    bool iniciarMapa();
    bool iniciarMapa(){

      cout << "Amplie a Tela para Melhor Visualização" << endl;
      cout << "Digite Apenas uma Tecla por Vez para Melhor Resolução" << endl;
    	cout << "Pressione qualquer tecla para criar gerações. Para sair digite 's' " << endl;
      cout << "Tecla Digitada: ";
      char teclas;
    	cin >> teclas;

    	while(teclas != 's'){

    		return true;
    	}

    	return 0;
    }

    int main(int argc, char const *argv[]) {
        GeradorFormas gerador;
        GlinderGun gun;
        Block block;
        Blinker blinker;
        Glider glider;

        int opcao;

        do{

        cout << "\t\t\t\tCONWAY'S GAME OF LIFE" << endl;
        cout << "ESCOLHA SUA OPÇÃO: " << endl;
        cout << "1 - BLOCK" << endl;
        cout << "2 - BLINKER" << endl;
        cout << "3 - GLINDER" << endl;
        cout << "4 - GOSPER GLINDER GUN" << endl;
        cout << "5 - REGRAS DO JOGO" << endl;
        cout << "6 - COMO FUNCIONA" << endl;
        cout << "7 - CRÉDITOS" << endl;
        cout << "QUALQUER OUTRA TECLA - SAIR" <<  endl;
        cout << "Opção: ";
        cin >> opcao;

        if (opcao == 1){
    			block.block();
    			block.gerarForma();
    			block.limparTela();
    			cout << endl;
    		}
        if (opcao == 2){
    		Posicao posicaoI = getPosicao();
    			blinker.blinker(posicaoI);
    			blinker.gerarForma();
    			while(iniciarMapa()){
    				blinker.Vizinhos();
    				blinker.gerarForma();
    			}
    			blinker.limparTela();
    		}
    		if (opcao == 3){
    			Posicao posicaoI = getPosicao();
    			glider.glider(posicaoI);
    			glider.gerarForma();
    			while(iniciarMapa()){
    				glider.Vizinhos();
    				glider.gerarForma();
    			}
    			glider.limparTela();
    		}
    		if (opcao == 4){
    			gun.gun();
    			gun.gerarForma();
    			while(iniciarMapa()){
    				gun.Vizinhos();
    				gun.gerarForma();
    			}
    			gun.limparTela();
    		}

      if (opcao == 5){
        cout << "\t\t\t\tCONWAY'S GAME OF LIFE" << endl;
        cout << "\t\t\t\tREGRAS DO JOGO" << endl;
        cout << "1) Qualquer célula viva com menos de dois vizinhos vivos morre de solidão. "<< endl;;
        cout << "2) Qualquer célula viva com mais de três vizinhos vivos morre de superpopulação." << endl;
        cout << "3) Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva." << endl;
        cout << "4) Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geração." << endl<< endl;
        cout << endl;
      }

      if (opcao == 6){
          cout << "\t\t\t\tCONWAY'S GAME OF LIFE" << endl;
          cout << "\t\t\t\t COMO FUNCIONA" << endl;
          cout << " - O Jogo trabalha com uma Matriz já definida de 50 linhas e 100 colunas" << endl;
          cout << " - Você pode escolher entra as opções definadas no Menu" << endl;
          cout << " - Em algumas opções pode ser pedido para digitar tal caracter para geração do programa" << endl;
          cout << " - O Jogo funciona a base de dígitos, pode digitar qualquer teclas dos tipos letras, números e símbolos para funcionar" << endl;
          cout << " - Aumenta a tela do compilador para vizualizar adequadamente o programa" << endl;
          cout << " - Para mais detalhes do Jogo leia o README na pasta DOC"<< endl;
          cout << endl;
        }

      if (opcao == 7){
        cout << "\t\t\t\tCONWAY'S GAME OF LIFE" << endl;
        cout << "ALUNO: João Lucas Sousa Reis" << endl;
        cout << "MATRÍCULA: 160009758" << endl;
        cout << "Segundo Semestre de 2017" << endl;
        cout << "Orientação a Objetos - UNB-GAMA(FGA)"<< endl << endl;
        cout << endl;
      }
}
      while (opcao == 1 || opcao == 2 || opcao == 3 || opcao == 4 || opcao == 5 || opcao == 6 || opcao == 7);

      return 0;
    }
