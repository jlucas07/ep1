#include "geradorformas.hpp"
#include <iostream>

  GeradorFormas::GeradorFormas(){
    // PREENCER COM ESPAÇOS VAZIOS
    for(int i=0; i<50; i++) {
        for(int j=0; j<100; j++) {
            gerador[i][j] = 0;
        }
    }
  }

    GeradorFormas::~GeradorFormas(){}

  void GeradorFormas::Vizinhos(){

    bool geradorMapa[50][100];

    for(int i=0; i<50; i++) {
        for(int j=0; j<100; j++) {
            geradorMapa[i][j] = 0;
    }
  }

    for(int i=0; i<49; i++) {
        for(int j=0; j<100; j++) {
          int num_vizinhos = 0;
          if (gerador[i-1][j-1]){
            num_vizinhos++;
          }
          if (gerador[i+1][j+1]){
            num_vizinhos++;
          }
          if (gerador[i-1][j+1]){
            num_vizinhos++;
          }
          if (gerador[i+1][j-1]){
            num_vizinhos++;
          }
          if (gerador[i][j+1]){
            num_vizinhos++;
          }
          if (gerador[i][j-1]){
            num_vizinhos++;
          }
          if (gerador[i+1][j]){
            num_vizinhos++;
          }
          if (gerador[i-1][j]){
            num_vizinhos++;
          }

           if (num_vizinhos== 2 && gerador[i][j]){
            geradorMapa[i][j] = true;
          }
          else if (num_vizinhos< 2 || num_vizinhos > 3){
            geradorMapa[i][j] = 0;
          }

          else if (num_vizinhos == 3){
            geradorMapa[i][j] = true;
          }
        }
      }

      for (int i = 0; i < 50; ++i){
        for (int j = 0; j < 100; ++j){
          gerador[i][j] = geradorMapa[i][j];
        }
    }
  }

  void GeradorFormas::limparTela(){

    for (int i = 0; i < 50; i++){
      for (int j = 0; j < 100; j++){
        gerador[i][j] = 0;
        }
      }
  }

  void GeradorFormas::gerarForma(){

    for (int i = 0; i < 50; ++i){

      for (int j = 0; j < 100; ++j){

        if (gerador[i][j] == true){
          cout << 'X';
        }

        else {

          cout << '-';
        }
      }
      
      cout << endl;
    }
  }
